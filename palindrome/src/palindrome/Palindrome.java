package palindrome;

import sun.lwawt.macosx.CSystemTray;

public class Palindrome {
    public static boolean isPalindrome(String text) {
        return (text.equals(reverseString(text)));
    }

    public static String reverseString (String text) {
        StringBuilder reverse = new StringBuilder();
        String clean = text.replaceAll("\\s+", "").toLowerCase();
        char[] plain = clean.toCharArray();
        for (int i = plain.length - 1; i >= 0; i--) {
            reverse.append(plain[i]);
        }
    return reverse.toString();
    }
    public static void printOut (String palindrome){
        if (isPalindrome(palindrome)){
        System.out.print(palindrome + " is palindrome");}
        else {
            System.out.print(palindrome + " is not palindrome");
        }

    }
}
